<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('model_couple');
	}

	public function index()
	{
		//$this->load->helper('url');
		$data['title'] = 'Find Your True Love';
		$data['value'] = '0%';
		$this->load->view('teras',$data);
	}

	public function getResult(){
		$namakesatu = $this->input->post('FirstName');
		$namakedua = $this->input->post('SecondName');
		$now = date('Y-m-d H:i:s');
		$data = array(
			'tanggal' => $now,
			'nama1' => $namakesatu,
			'nama2' => $namakedua,
			'hasil' => $this->Process($namakesatu,$namakedua));
		
		if($this->model_couple->insert($data))
		/*echo "<script src=".base_url()."assets/js/jquery-1.11.1.min.js type=text/javascript></script>";
		echo "<script type='text/javascript'> 
		       $(document).ready(function(){
		           $('#thankyouModal').modal('show');
		       });
		      </script>";
		     echo "<div class=modal fade id=thankyouModal tabindex=-1 role=dialog aria-labelledby=thankyouLabel aria-hidden=true>
    <div class=modal-dialog>
        <div class=modal-content>
            <div class=modal-header>
                <button type=button class=close data-dismiss=modal aria-hidden=true>&times;</button>
                <h4 class=modal-title id=myModalLabel>Thank you for pre-registering!</h4>
            </div>
            <div class=modal-body>
                <p>You'll be the first to know when Shopaholic launches.</p>                     
                <p>In the meantime, any <a href=http://shopaholic.uservoice.com/ target=_blank>feedback</a> would be much appreciated.</p>
            </div>    
        </div>
    </div>
</div>";*/
		echo "berhasil";
		//$this->index();
	}

	private function process($FirstName,$SecondName) {
		$Name = strtolower($FirstName)."".strtolower($SecondName);
		//echo "$Name";
		$nilai = array();
		$key = 'truelove';
		for ($i=0; $i < 8; $i++) { 
			$temp = 0;
			for ($j=0; $j < strlen($Name); $j++) { 
				if ($Name[$j]==$key[$i]) $temp++;
			}
			$nilai[$i] = $temp;
		}
		$maxloop = 8;
		for ($i=0; $i < 6; $i++) { 
			for ($j=0; $j < ($maxloop-1); $j++) { 
				$nilai[$j] = ($nilai[$j] + $nilai[$j+1]) % 10;
			}
			$maxloop--;
		}
		return (int)$nilai[0]."".$nilai[1];	
	}
}
