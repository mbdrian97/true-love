<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title><?php echo $title;?></title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Manufactory Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<!-- //js -->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/move-top.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});

		/*$('#form_couple').submit(function(event){
			event.preventDefault();
			var nama1 = $('#nama1').val();
			var nama2 = $('#nama2').val();

			$.ajax({
			        type: 'POST',
			        url: <?php echo site_url('welcome/getResult')?>,
			        data: {
			         'FirstName': nama1,
			         'SecondName': nama2

			        },
			        dataType: 'html',
			        success: function(results){
			             <?php echo "berhasil"; ?>
			        }
			  });
		});*/

	});

</script>
<!-- start-smoth-scrolling -->

<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Montserrat+Alternates:400,700' rel='stylesheet' type='text/css'>

<link href="<?php echo base_url();?>assets/images/fav.png" rel="shortcut icon">


</head>
	
<body>
<!-- banner -->
	<div class="banner">
		<div class="container">
			<div class="header">
				<div class="banner-left">
					<!-- <a href="index.html"><i class="glyphicon glyphicon-wrench" aria-hidden="true"></i>Manu<span>Factory</span></a> -->
					<a href="<?php echo site_url();?>"><img src="<?php echo base_url();?>assets/images/logo.png"></a>
				</div>
				<!-- <div class="main-in">
					<section>
						<button id="showLeft" class="navig"></button>
					</section>
				</div> -->
				<div class="clearfix"> </div>
					<!--- Navigation from Right To Left --->
					<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/component.css" />
						<script type="text/javascript">
				
						  var _gaq = _gaq || [];
						  _gaq.push(['_setAccount', 'UA-7243260-2']);
						  _gaq.push(['_trackPageview']);
				
						  (function() {
							var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
							ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
							var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
						  })();
				
						</script>
						<div class="cbp-spmenu-push">
							<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
								<h3>Menu</h3>
								<a href="#home" class="scroll">Home</a>
								<a href="#services" class="scroll">Services</a>
								<a href="#about" class="scroll">About Us</a>
								<a href="#news" class="scroll">News & Events</a>
								<a href="#contact" class="scroll">Contact</a>
							</nav>
					</div>
						<script src="<?php echo base_url();?>assets/js/classie.js"></script>
						<script>
							var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),				
								showLeft = document.getElementById( 'showLeft' ),				
								body = document.body;

							showLeft.onclick = function() {
								classie.toggle( this, 'active' );
								classie.toggle( menuLeft, 'cbp-spmenu-open' );
								disableOther( 'showLeft' );
							};
							function disableOther( button ) {
								if( button !== 'showLeft' ) {
									classie.toggle( showLeft, 'disabled' );
								}
							}
						</script>
					<!--- Navigation from Right To Left --->
				<div class="clearfix"> </div>
			</div>
			<div class="banner-info">
				<div class="banner-info-left">
					<h1>True Love</h1>
					<form method='post' action='<?php echo site_url('welcome/getResult');?>' autocomplete="off">
						<input id='nama1' name='FirstName' type="text" value="Nama Anda" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Nama Anda';}" required="">
						<input id='nama2' name='SecondName' type="text" value="Nama Pasangan Anda" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Nama Pasangan Anda';}" required="">
						<input type="submit" value="Submit" class='backgroud-button'>
					</form>
				</div>

				<div class="banner-info-right">
					<h2>Temukan pasangan terbaik anda</h2>
					<p>Masukkan nama anda dan nama pasangan anda untuk mengetahui berapa persen kecocokan anda.
						It's just for fun. <br>- <br>Rian</p>
					<ul>
						<li><a href="https://www.facebook.com/badriansyah.rian" class="facebook"> </a></li>
						<li><a href="https://twitter.com/mbdrian" class="twitter"> </a></li>
					</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //banner -->
<!-- banner-bottom -->
	<!-- <div class="banner-bottom">
		<div class="container">
			<h3>Welcome !</h3>
			<p class="fugiat">Quis autem vel eum iure reprehenderit qui in ea voluptate velit 
				esse quam nihil molestiae consequatur, vel illum qui dolorem eum 
				fugiat quo voluptas nulla pariatur</p>
			<h4>Our Latest Projects</h4>
			<div class="banner-bottom-grid">
				<div class="col-md-6 banner-bottom-grid-left">
					<img src="images/1.png" alt=" " class="img-responsive" />
				</div>
				<div class="col-md-6 banner-bottom-grid-right">
					<h5>esse quam nihil molestiae consequatur</h5>
					<p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit 
						esse quam nihil molestiae consequatur, vel illum qui dolorem eum 
						fugiat quo voluptas nulla pariatur</p>
					<div class="banner-b-left">
						<div class="banner-b-left1">
							<img src="images/1.jpg" alt=" " class="<?php echo base_url();?>assets/img-responsive" />
						</div>
						<div class="banner-b-left1">
							<img src="images/2.jpg" alt=" " class="<?php echo base_url();?>assets/img-responsive" />
						</div>
						<div class="banner-b-left1">
							<img src="images/3.jpg" alt=" " class="<?php echo base_url();?>assets/img-responsive" />
						</div>
						<div class="banner-b-left1">
							<img src="images/4.jpg" alt=" " class="<?php echo base_url();?>assets/img-responsive" />
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="banner-b-left">
						<div class="banner-b-left1">
							<img src="images/4.jpg" alt=" " class="<?php echo base_url();?>assets/img-responsive" />
						</div>
						<div class="banner-b-left1">
							<img src="images/3.jpg" alt=" " class="<?php echo base_url();?>assets/img-responsive" />
						</div>
						<div class="banner-b-left1">
							<img src="images/2.jpg" alt=" " class="<?php echo base_url();?>assets/img-responsive" />
						</div>
						<div class="banner-b-left1">
							<img src="images/1.jpg" alt=" " class="<?php echo base_url();?>assets/img-responsive" />
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div> -->
<!-- //banner-bottom -->
<!-- about -->
	<!-- <div class="about" id="about">
		<div class="container">
			<h3>About Us</h3>
			<p class="fugiat colr">Quis autem vel eum iure reprehenderit qui in ea voluptate velit 
				esse quam nihil molestiae consequatur, vel illum qui dolorem eum 
				fugiat quo voluptas nulla pariatur</p>
			<div class="about-grid">
				<div class="col-md-6 about-grid-left">
					<img src="<?php echo base_url();?>assets/images/4.png" alt=" " class="img-responsive" />
					<h4>magnam aliquam quaerat voluptatem</h4>
					<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, 
						consectetur, adipisci velit, sed quia non numquam eius modi tempora
						incidunt ut labore et dolore magnam aliquam quaerat.</p>
				</div>
				<div class="col-md-6 about-grid-right">
					<h4>Poll</h4>
					<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, 
						consectetur, adipisci velit.</p>
					<div class="progress">
						<div class="progress-bar p" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
							60%
						</div>
					</div>
					<div class="progress">
						<div class="progress-bar p" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">
							70%
						</div>
					</div>
					<div class="progress">
						<div class="progress-bar p" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">
							80%
						</div>
					</div>
					<div class="progress">
						<div class="progress-bar p" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">
							90%
						</div>
					</div>
					<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, 
						consectetur, modi tempora incidunt ut labore et dolore magnam
						aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum 
						exercitationem ullam corporis suscipit laboriosam, nisi 
						ut aliquid ex ea commodi consequatur adipisci velit.</p>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div> -->
<!-- //about -->
<!-- what-we-do -->
	<!-- <div class="what-we-do">
		<div class="container">
			<h3>What We Do !</h3>
			<p class="fugiat">Quis autem vel eum iure reprehenderit qui in ea voluptate velit 
				esse quam nihil molestiae consequatur, vel illum qui dolorem eum 
				fugiat quo voluptas nulla pariatur</p>
			<div class="what-we-do-grids">
				<div class="col-md-3 what-we-do-grid">
					<img src="<?php echo base_url();?>assets/images/5.png" alt=" " class="img-responsive" />
					<h4>esse quam nihil molestiae consequatur</h4>
					<ul>
						<li><a href="#" class="w"> </a></li>
						<li><a href="#" class="w1"> </a></li>
						<li><a href="#" class="w2"> </a></li>
						<li><a href="#" class="w3"> </a></li>
					</ul>
					<p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit 
						esse quam nihil molestiae consequatur.</p>
				</div>
				<div class="col-md-3 what-we-do-grid">
					<img src="<?php echo base_url();?>assets/images/6.png" alt=" " class="img-responsive" />
					<h4>esse quam nihil molestiae consequatur</h4>
					<ul>
						<li><a href="#" class="w"> </a></li>
						<li><a href="#" class="w1"> </a></li>
						<li><a href="#" class="w2"> </a></li>
						<li><a href="#" class="w3"> </a></li>
					</ul>
					<p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit 
						esse quam nihil molestiae consequatur.</p>
				</div>
				<div class="col-md-3 what-we-do-grid">
					<img src="<?php echo base_url();?>assets/images/7.png" alt=" " class="img-responsive" />
					<h4>esse quam nihil molestiae consequatur</h4>
					<ul>
						<li><a href="#" class="w"> </a></li>
						<li><a href="#" class="w1"> </a></li>
						<li><a href="#" class="w2"> </a></li>
						<li><a href="#" class="w3"> </a></li>
					</ul>
					<p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit 
						esse quam nihil molestiae consequatur.</p>
				</div>
				<div class="col-md-3 what-we-do-grid">
					<img src="<?php echo base_url();?>assets/images/8.png" alt=" " class="img-responsive" />
					<h4>esse quam nihil molestiae consequatur</h4>
					<ul>
						<li><a href="#" class="w"> </a></li>
						<li><a href="#" class="w1"> </a></li>
						<li><a href="#" class="w2"> </a></li>
						<li><a href="#" class="w3"> </a></li>
					</ul>
					<p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit 
						esse quam nihil molestiae consequatur.</p>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div> -->
<!-- //what-we-do -->
<!-- team -->
	<!-- <div class="team" id="team">
		<div class="container">
			<h3>Meet Our Team</h3>
			<p class="fugiat">Quis autem vel eum iure reprehenderit qui in ea voluptate velit 
				esse quam nihil molestiae consequatur, vel illum qui dolorem eum 
				fugiat quo voluptas nulla pariatur</p>
			<div class="team-grids">
				<section class="main">
					<ul class="ch-grid">
						<li>
							<div class="ch-item ch-img-1">
								<div class="ch-info">
									<h3>Industrier</h3>
									<p>by Daniel Nyari <a href="#">View on Dribbble</a></p>
								</div>
							</div>
						</li>
						<li>
							<div class="ch-item ch-img-2">
								<div class="ch-info">
									<h3>Industrier</h3>
									<p>by Daniel Nyari <a href="#">View on Dribbble</a></p>
								</div>
							</div>
						</li>
						<li>
							<div class="ch-item ch-img-3">
								<div class="ch-info">
									<h3>Industrier</h3>
									<p>by Daniel Nyari <a href="#">View on Dribbble</a></p>
								</div>
							</div>
						</li>
					</ul>
				</section>
			</div>
		</div>
	</div> -->
<!-- //team -->
<!-- news -->
	<!-- <div class="news" id="news">
		<div class="container">
			<h3>News & Events</h3>
			<p class="fugiat colr">Quis autem vel eum iure reprehenderit qui in ea voluptate velit 
				esse quam nihil molestiae consequatur, vel illum qui dolorem eum 
				fugiat quo voluptas nulla pariatur</p>
			<div class="news-grid">
				<div class="col-md-4 news-grid-left">
					<div class="news-grid-left-top">
						<p><i class="glyphicon glyphicon-calendar" aria-hidden="true"></i><span>August 15th 2015 at 10:30 AM</span></p>
					</div>
					<img src="<?php echo base_url();?>assets/images/5.png" alt=" " class="img-responsive" />
					<div class="news-pos">
						<p>vel illum qui dolorem eum 
							fugiat quo voluptas nulla pariatur</p>
					</div>
				</div>
				<div class="col-md-4 news-grid-left">
					<div class="news-grid-left-top">
						<p><i class="glyphicon glyphicon-calendar" aria-hidden="true"></i><span>August 15th 2015 at 10:30 AM</span></p>
					</div>
					<img src="<?php echo base_url();?>assets/images/8.png" alt=" " class="img-responsive" />
					<div class="news-pos">
						<p>vel illum qui dolorem eum 
							fugiat quo voluptas nulla pariatur</p>
					</div>
				</div>
				<div class="col-md-4 news-grid-left">
					<div class="news-grid-left-top">
						<p><i class="glyphicon glyphicon-calendar" aria-hidden="true"></i><span>August 15th 2015 at 10:30 AM</span></p>
					</div>
					<img src="<?php echo base_url();?>assets/images/7.png" alt=" " class="img-responsive" />
					<div class="news-pos">
						<p>vel illum qui dolorem eum 
							fugiat quo voluptas nulla pariatur</p>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div> -->
<!-- //news -->
<!-- testimonials -->
	<!-- <div class="customer">
		<div class="container">
			<h3>Customer Says</h3>
			<p class="fugiat">Quis autem vel eum iure reprehenderit qui in ea voluptate velit 
				esse quam nihil molestiae consequatur, vel illum qui dolorem eum 
				fugiat quo voluptas nulla pariatur</p>
				<div class="wmuSlider example1">
					<div class="wmuSliderWrapper">
						<article style="position: absolute; width: 100%; opacity: 0;"> 
						<div class="banner-wrap">
							<div class="testimonials-grid">
								<div class="col-md-6 testimonials-grid-left">
									<div class="testimonials-grid-left1">
										<img src="<?php echo base_url();?>assets/images/11.png" alt=" " class="img-responsive" />
									</div>
									<div class="testimonials-grid-right1">
										<p><span>L</span>orem ipsum dolor sit amet, consectetur adipiscing elit, 
											sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
											Ut enim ad minim veniam.</p>
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="col-md-6 testimonials-grid-left">
									<div class="testimonials-grid-left1">
										<img src="<?php echo base_url();?>assets/images/10.png" alt=" " class="img-responsive" />
									</div>
									<div class="testimonials-grid-right1">
										<p><span>L</span>orem ipsum dolor sit amet, consectetur adipiscing elit, 
											sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
											Ut enim ad minim veniam.</p>
									</div>
									<div class="clearfix"> </div>
								</div>
							</div>
						</div>
						</article>
						<article style="position: absolute; width: 100%; opacity: 0;"> 
						<div class="banner-wrap">
							<div class="testimonials-grid">
								<div class="col-md-6 testimonials-grid-left">
									<div class="testimonials-grid-left1">
										<img src="<?php echo base_url();?>assets/images/12.png" alt=" " class="img-responsive" />
									</div>
									<div class="testimonials-grid-right1">
										<p><span>L</span>orem ipsum dolor sit amet, consectetur adipiscing elit, 
											sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
											Ut enim ad minim veniam.</p>
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="col-md-6 testimonials-grid-left">
									<div class="testimonials-grid-left1">
										<img src="<?php echo base_url();?>assets/images/10.png" alt=" " class="img-responsive" />
									</div>
									<div class="testimonials-grid-right1">
										<p><span>L</span>orem ipsum dolor sit amet, consectetur adipiscing elit, 
											sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
											Ut enim ad minim veniam.</p>
									</div>
									<div class="clearfix"> </div>
								</div>
							</div>
						</div>
						</article>
						<article style="position: absolute; width: 100%; opacity: 0;"> 
						<div class="banner-wrap">
							<div class="testimonials-grid">
								<div class="col-md-6 testimonials-grid-left">
									<div class="testimonials-grid-left1">
										<img src="<?php echo base_url();?>assets/images/11.png" alt=" " class="img-responsive" />
									</div>
									<div class="testimonials-grid-right1">
										<p><span>L</span>orem ipsum dolor sit amet, consectetur adipiscing elit, 
											sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
											Ut enim ad minim veniam.</p>
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="col-md-6 testimonials-grid-left">
									<div class="testimonials-grid-left1">
										<img src="<?php echo base_url();?>assets/images/12.png" alt=" " class="img-responsive" />
									</div>
									<div class="testimonials-grid-right1">
										<p><span>L</span>orem ipsum dolor sit amet, consectetur adipiscing elit, 
											sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
											Ut enim ad minim veniam.</p>
									</div>
									<div class="clearfix"> </div>
								</div>
							</div>
						</div>
						</article>
					</div>
				</div>
				<script src="<?php echo base_url();?>assets/js/jquery.wmuSlider.js"></script> 
					<script>
						$('.example1').wmuSlider();         
					</script> 
		</div>
	</div> -->
<!-- //testimonials -->
<!-- services -->
	<!-- <div class="services" id="services">
		<div class="container">
			<h3>Services</h3>
			<p class="fugiat">Quis autem vel eum iure reprehenderit qui in ea voluptate velit 
				esse quam nihil molestiae consequatur, vel illum qui dolorem eum 
				fugiat quo voluptas nulla pariatur</p>
			<div class="service-grids">
				<div class="col-md-4 service-grid">
					<div class="service-icon">
						<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
					</div>
					<h4>Neque porro quisquam est, qui dolorem</h4>
					<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa 
						qui officia deserunt mollit anim id est laborum.</p>
					<div class="more">
						<a href="#">Read More</a>
					</div>
				</div>
				<div class="col-md-4 service-grid">
					<div class="service-icon">
						<span class="glyphicon glyphicon-fire" aria-hidden="true"></span>
					</div>
					<h4>Neque porro quisquam est, qui dolorem</h4>
					<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa 
						qui officia deserunt mollit anim id est laborum.</p>
					<div class="more">
						<a href="#">Read More</a>
					</div>
				</div>
				<div class="col-md-4 service-grid">
					<div class="service-icon">
						<span class="glyphicon glyphicon-star" aria-hidden="true"></span>
					</div>
					<h4>Neque porro quisquam est, qui dolorem</h4>
					<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa 
						qui officia deserunt mollit anim id est laborum.</p>
					<div class="more">
						<a href="#">Read More</a>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div> -->
<!-- //services -->
<!-- contact -->
	<div id="contact" class="contact">
	<div class="container">
		<h3>About this app</h3>
		<p class="fugiat">Sekali lagi, aplikasi ini hanya untuk bersenang-senang. Terima kasih kepada 
		<a href="https://www.facebook.com/rajihcc">Rajih</a> dan 
		<a href="https://www.facebook.com/fernandoaditya.petronaz">Adit</a> yang telah memberikan ide gila berserta algoritma-nya.
		</p>
		<div class="contact-us1-bottom">
			<!-- <form>
				<input type="text" value="Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}" required="">
				<input type="email" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
				<textarea type="text"  onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message...';}" required="">Message...</textarea>
				<input type="submit" value="Submit" >
			</form> -->
			<!-- <div class="home-radio-clock">
				<ul>
					<li><i class="glyphicon glyphicon-home" aria-hidden="true"></i>Our Home</li>
					<li><i class="glyphicon glyphicon-bullhorn" aria-hidden="true"></i>Call Us</li>
					<li><i class="glyphicon glyphicon-time" aria-hidden="true"></i>Opening Time</li>
				</ul>
			</div>
			<div class="home-radio-clock-right">
				<ul>
					<li>4978 Diamond Circle
						<span>Washington, NJ 07087</span></li>
					<li class="lst">(012) 123-456-78</li>
					<li>(012) 123-456-78<span>Mon-Fri:</span><span>09h-17h</span></li>
				</ul>
			</div> -->
			<div class="clearfix"> </div>
		</div>
		<!-- <div class="map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3104.2960728675243!2d-77.0274216!3d38.9172098!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89b7b7e5dea825b5%3A0x9e687c7e41a2d84e!2sIndustrial+Bank!5e0!3m2!1sen!2sin!4v1439461413522" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div> -->
	</div>
	</div>
<!-- //contact -->
<!-- footer -->
	<div class="footer">
	<div class="container">
		<div class="footer-left">
			<p>© 2015 Manufactory. All rights reserved | Design by <a href="http://w3layouts.com/"> W3layouts</a></p>
		</div>
		<div class="footer-right">
			<ul>
				<li><a href="https://www.facebook.com/badriansyah.rian" class="f"> </a></li>
				<li><a href="https://twitter.com/mbdrian" class="f1"> </a></li>
			</ul>
		</div>
		<div class="clearfix"> </div>
	</div>
	</div>
<!-- //footer -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->

<!-- <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div> -->
   <script>
    /*$( document ).ready(function() {
        // Handler for .ready() called.
        console.log("document ready");
        $('#test').modal({show:true});
    });*/
   </script>
</body>
</html>