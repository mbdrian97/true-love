<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>True Love</title>
    <!--File css-->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <!--Icon Tab-->
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/love.png" />
  </head>
  <body>
    <div class="wrapper">
		<div class="container">
			<div class='left'>
				<h1>Love Tester</h1>
				<h3>Let's find out how much he or she love you</h3>
				<form class="form"  action="<?php echo site_url('Result');?>" method='POST'>
					<input type="text" placeholder="Your Name" name='FirstName'>
					<input type="text" placeholder="Your Couple Name" name='SecondName'>
					<input type='submit' id='tombol' value='Get Result'></input>
				</form>
			</div>
			<div class='right'>
				<h1>Result</h1>
				<div class='results'><div id='hasil'><?php echo $value;?></div></div>
				
			</div>
		</div>
		<div class='share'>
			<h2>Bagikan Cinta</h2>
			<div class='medsos'>
				<a href="http://www.facebook.com/sharer.php?u=https://simplesharebuttons.com" target="_blank">
					<img src='<?php echo base_url();?>assets/img/fb.png' alt='Facebook'/>
				</a>
				<a href="https://twitter.com/share?url=https://simplesharebuttons.com&amp;name=Simple Share Buttons&amp;hashtags=simplesharebuttons" target="_blank">
					<img src='<?php echo base_url();?>assets/img/tw.png' alt='Twitter'/>
				</a>
				<a href="https://plus.google.com/share?url=https://simplesharebuttons.com" target="_blank">
					<img src='<?php echo base_url();?>assets/img/gp.png' alt='Google Plus'/>
				</a>
			</div>
		</div>
		<ul class="bg-bubbles">
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
		</ul>
	</div>
  	<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
	<script src="<?php echo base_url();?>assets/js/index.js"></script>
  </body>
</html>
