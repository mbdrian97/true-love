<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_couple extends CI_Model {
    private $table = 'tabel_couple';

    public function get_table() {
        return $this->table;
    } 

    public function insert($data) {
        $table = $this->get_table();

        return $this->db->insert($table, $data);
    }
}